# Clock application for URSI

React application for simulation

## How to run

In the project directory, run:

### `npm install`

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## How can applications test this
If you want to test this clock with your own application, you must edit `App.js` line 179:
Change this
```angular2
let apps = ["back-office-magasin", "caisse", "decisionnel", "e-commerce", "gestion-commerciale", "gestion-entrepot", "gestion-promotion", "monetique-paiement", "relation-client", "referenciel-produit"];
```

to only include your app name.

Then run the application, make sure the registration works before starting the clock.

## CORS error
If you run into a CORS error, you need to go to this link for the [Kong Admin interface](http://localhost:1337)
 - Register an admin account (it's only available locally so use whatever you want)
 - Log in
 - On the left, go on `Connections` then add a `New Connection` with the following: 
     - Name: Kong
     - Kong Admin URL: http://kong:8081
 - On the right, `Activate` the connection
 - On the left, go on `Plugins` then `Add Global Plugins`
 - On the `Security` tab add `Cors`
 - Write `*` in `Headers` and `Origins
 - `Add Plugin`
 
 ![](cors_plugin.png "Cors")

## Authors

* **Equipe Socle technique URSI SIGL2021** - *Initial work* 

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
