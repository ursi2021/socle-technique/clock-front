import React from "react";
import "./Clock.css";

import Hand from "./Hand";
import Digital from "./Digital";

export default function ClockReact(props) {
    const [hh, setHh] = React.useState(0);
    const [mm, setMm] = React.useState(0);
    const [ss, setSs] = React.useState(0);

    const units = {
        hour: {
            name: "hour",
            short: "hr",
            divider: 12
        },
        minute: {
            name: "min",
            short: "mn",
            divider: 30
        },
        second: {
            name: "sec",
            short: "sc",
            divider: 6
        }
    };

    React.useEffect(() => {
        setInterval(() => {
            const now = props.getSimulatedDate();
            setHh(now.getHours() * 30);
            setMm(now.getMinutes() * units.second.divider);
            setSs((now.getTime() / 1000) * units.second.divider);
            // setSs(now.getSeconds() * deg);
        }, 10);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="clock">
            {props.digital && <Digital time={{ hh, mm }} />}
            <Hand time={{ hh, mm, ss }} unit={units.hour} />
            <Hand time={{ hh, mm, ss }} unit={units.minute} />
            {/*<Hand time={{ hh, mm, ss }} unit={units.second} />*/}
        </div>
    );
}
