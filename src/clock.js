import axios from "axios";

let today = new Date();

let origin = new Date('2021-01-04T00:00:00');
console.log("Simulation start date: " + origin);
let speed = 3600 * 10;


// let obj = {
//     j: 0, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
//     h: 0, // required, must be valid
//     m: 0, // required, must be valid
//     date: '', // Date format if j != null, else  null.
//     name: '', // required
//     route: '' // required
// };

let schedule = [
    {
        j: 0, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
        h: 12, // required, must be valid
        m: 0, // required, must be valid
        date: null, // Date format if j != null, else  null.
        name: 'Every day at noon', // required
        route: 'localhost' // required
    },
    {
        j: null, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
        h: 5, // required, must be valid
        m: 30, // required, must be valid
        date: new Date('2021-01-04'), // Date format if j != null, else  null.
        name: '5AM on Monday 4th', // required
        route: 'localhost' // required
    },
    {
        j: 1, //index for day of the week. Monday at 1, Sunday at 7, 0 is everyday, null for specific date
        h: 14, // required, must be valid 0 - 23
        m: 42, // required, must be valid 0 - 59
        date: null, // Date format if j != null, else  null.
        name: 'Every Monday Treatment', // required
        route: 'localhost' // required
    },
];

let url = "kong"; //TODO
let apps=[]; //TODO

function getRegistration() {
    for (let a in apps) {
        axios.get(url + "/" + a + "/clock-register").then(function (response) {
            // handle success
            schedule.push(response.data);
        }).catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    schedule.map(value => { value.executed = false});
}

let resetDaily = false;
let resetWeekly = false;

function getElapsedTime() {
    let elapsedTime = (new Date() - today) * speed;

    let simulatedTime = new Date(origin.getTime() + elapsedTime);
    console.log("Current simulated time: " + simulatedTime);

    if (simulatedTime.getHours() === 0) {
        schedule.filter(item => item.j === 0).map( value => value.executed = false) ;
        resetDaily = true;
        if (!resetWeekly && simulatedTime.getDay() === 0) {
            schedule.filter(item => item.j !== null).map( value => value.executed = false );
            resetWeekly = true
        } else {
            resetWeekly = false;
        }
    } else {
        resetDaily = false;
    }

    schedule.forEach(item => {
        if (!item.executed) {
            if (item.j === 0) { //everyday
                if (simulatedTime.getHours() === item.h && simulatedTime.getMinutes() >= item.m ) {
                    item.executed = true;
                    executeItem(item.name, item.route);
                }
            } else if (item.j === null) { // specific date
                if (simulatedTime.getDate() === item.date.getDate()
                    && simulatedTime.getMonth() === item.date.getMonth()
                    && simulatedTime.getFullYear() === item.date.getFullYear()) {
                    if (simulatedTime.getHours() === item.h && simulatedTime.getMinutes() >= item.m ) {
                        item.executed = true;
                        executeItem(item.name, item.route);
                    }
                }
            } else { // weekly
                if (simulatedTime.getDay() === item.j) {
                    if (simulatedTime.getHours() === item.h && simulatedTime.getMinutes() >= item.m ) {
                        item.executed = true;
                        executeItem(item.name, item.route);
                    }
                }
            }
        }
    });
}

function executeItem(name, route) {
    clearInterval(interval);
    let today2 = new Date();
    console.log("Executing: " + name + " get at " + route);

    // axios.get(url + "/" + route).then(function (response) {
    //     // handle success
    //     console.log(response.data);
    // }).catch(function (error) {
    //     // handle error
    //     console.log(error);
    // });

    let elapsed = new Date() - today2;
    today = new Date((1 * today) + elapsed);
    interval = setInterval(getElapsedTime, 60 * 1000 / speed);

}

getRegistration();
let interval = setInterval(getElapsedTime, 60 * 1000 / speed);

