import React, {Component} from "react";

import {Col, Container, Input, Row} from "reactstrap";
import axios from "axios";
import ClockReact from "./ClockReact";
import {Console, Decode, Hook} from "console-feed";


class App extends Component {

    constructor(props) {
        super(props);
        this.stopSimulation = this.stopSimulation.bind(this);
        this.startSimulation = this.startSimulation.bind(this);
        this.getSimulation = this.getSimulation.bind(this);
        this.resetSimulation = this.resetSimulation.bind(this);
        this.getSimulatedDate = this.getSimulatedDate.bind(this);
        this.changeSpeed = this.changeSpeed.bind(this);

        this.state = {
            simulatedTime: new Date('2021-01-04T00:00:00'),
            speed: 36000,
            pause: true,
            state: null,
            logs: [],
        };
    }

    interval = 100;
    resetDaily = false;
    resetWeekly = false;
    schedule = [];

    kongRegisterURL = process.env.REACT_APP_KONG_REGISTER_URL;
    kongURL = process.env.REACT_APP_KONG_URL;

    componentDidMount() {
        Hook(window.console, log => {
            this.setState(({logs}) => ({logs: [...logs, Decode(log)]}))
        });

        this.register_kong().then(() => {
            console.info("Clock Kong registration done");
            this.getRegistration();
        }).catch((error) => {
            console.info("Clock Kong Registration failed");
            console.error(error);
        });
    }

    async register_kong() {
        try {
            await axios.get(this.kongRegisterURL + '/services/' + process.env.REACT_APP_APP_NAME);
        } catch {
            await axios
                .post(this.kongRegisterURL + '/services/', {
                    name: process.env.REACT_APP_APP_NAME,
                    url: 'http://' + process.env.REACT_APP_APP_NAME
                });

            await axios
                .post(this.kongRegisterURL + '/services/' + process.env.REACT_APP_APP_NAME + '/routes', {
                    paths: ["/" + process.env.REACT_APP_APP_NAME],
                    name: process.env.REACT_APP_APP_NAME
                });
        }
    }

    getSimulation() {
        let elapsedTime = this.interval * this.state.speed;

        let simulatedTime = new Date(this.state.simulatedTime.getTime() + elapsedTime);

        this.setState({simulatedTime: simulatedTime});

        if (simulatedTime.getHours() === 0) {
            this.schedule.filter(item => item.j === 0).map(value => value.executed = false);
            this.resetDaily = true;
            if (!this.resetWeekly && simulatedTime.getDay() === 0) {
                this.schedule.filter(item => item.j !== null).map(value => value.executed = false);
                this.resetWeekly = true
            } else {
                this.resetWeekly = false;
            }
        } else {
            this.resetDaily = false;
        }

        this.schedule.forEach(item => {
            if (!item.executed) {
                if (item.j === 0) { //everyday
                    if ((simulatedTime.getHours() >= item.h && simulatedTime.getMinutes() >= item.m) || simulatedTime.getHours() > item.h) {
                        item.executed = true;
                        this.executeItem(simulatedTime, item);
                    }
                } else if (item.j === null) { // specific date
                    if (simulatedTime.getDate() >= item.date.getDate()
                        && simulatedTime.getMonth() >= item.date.getMonth()
                        && simulatedTime.getFullYear() >= item.date.getFullYear()) {
                        if ((simulatedTime.getHours() >= item.h && simulatedTime.getMinutes() >= item.m) || simulatedTime.getHours() > item.h) {
                            item.executed = true;
                            this.executeItem(simulatedTime, item);
                        }
                    }
                } else { // weekly
                    if (simulatedTime.getDay() >= item.j) {
                        if ((simulatedTime.getHours() >= item.h && simulatedTime.getMinutes() >= item.m) || simulatedTime.getHours() > item.h) {
                            item.executed = true;
                            this.executeItem(simulatedTime, item);
                        }
                    }
                }
            }
        });
    }

    executeItem(date, item) {
        let dateString = this.getDateString(date);
        let timeString;
        if (date.getHours() < 10) {
            timeString = "0" + date.getHours();
        } else {
            timeString = date.getHours();
        }

        if (date.getMinutes() < 10) {
            timeString = timeString + ":0" + date.getMinutes();
        } else {
            timeString = timeString + ":" + date.getMinutes();
        }
        let here = this;
        this.stopSimulation(false);
        let url = this.kongURL + "/" + item.app;
        if (!item.route.startsWith("/")) {
            url = url + "/";
        }
        url = url + item.route;
        axios.post(url, {
            params: item.params,
            simulatedDate: here.state.simulatedTime
        }).then(function (response) {
            console.log(dateString + ", " + timeString + " | Executing: " + item.name + " from %c" + item.app, "color:" + item.color);
            here.startSimulation(false);
        }).catch(function (error) {
            here.setState({pause: true});
            console.log(dateString + ", " + timeString + " | FAILED: " + item.name + " from %c" + item.app, "color:" + item.color);
            console.log(error);
        });
    }

    startSimulation(button = true) {
        let interval = setInterval(this.getSimulation, this.interval);
        this.setState({
            interval: interval
        });
        if (button) {
            this.setState({pause: false});
        }
    }

    stopSimulation(button = true) {
        clearInterval(this.state.interval);
        this.setState({
            interval: null,
        });

        if (button) {
            this.setState({pause: true});
        }
    }

    resetSimulation() {
        if (this.state.interval !== null) {
            this.stopSimulation();
        }
        this.setState({simulatedTime: new Date('2021-01-04T00:00:00')});
    }

    changeSpeed(speed) {
        this.setState({speed: speed});
    };

    getRegistration() {
        let apps = ["back-office-magasin", "caisse", "decisionnel", "e-commerce", "gestion-commerciale", "gestion-entrepot", "gestion-promotion", "monetique-paiement", "relation-client", "referenciel-produit"]; //TODO
        let colors = ["#ff0000", "#8a786a", "#ff8000", "#ffff00", "#045E28", "#00ff00", "#00ffff", "#0040ff", "#8000ff", "#ff00bf"];
        let here = this;
        for (let i = 0; i < apps.length; i++) {
            let app = apps[i];

            axios.get(this.kongURL + "/" + app + "/clock-register").then(function (response) {
                for (let j = 0; j < response.data.length; j++) {
                    let item = response.data[j];
                    item.color = colors[i];
                    item.app = app;
                    item.executed = false;
                    here.schedule.push(item);
                }
                console.info("Register from %c" + app, "color:" + colors[i]);
            }).catch(function (error) {
                console.info("Register failed from %c" + app, "color:" + colors[i]);
                console.error(error);
            });
        }
    }


    getDateString(date) {
        let year = date.getFullYear();
        let n = date.getDate();
        const months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];

        const days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ];
        const dayName = days[date.getDay()];
        const monthName = months[date.getMonth()];
        return dayName + ", " + monthName + " " + n + ", " + year;
    }

    getSimulatedDate() {
        return this.state.simulatedTime;
    }


    render() {
        return (
            <main className="content">
                <h1 className="text-white text-uppercase text-center my-4">Clock simulation</h1>

                <div className="">
                    <Container fluid>
                        <Row>
                            <Col sm={3}>
                                <div className="">
                                    <Console logs={this.state.logs} variant="dark" filter={['info']}/>
                                </div>
                            </Col>
                            <Col sm={6}>
                                <div className="">
                                    <div className="col-md-6 col-sm-10 mx-auto p-0 d-flex justify-content-center ">
                                        <ClockReact digital getSimulatedDate={this.getSimulatedDate}/>
                                    </div>
                                    <div className="col-md-8 col-sm-10 mx-auto p-0 d-flex justify-content-center">
                                        <h1 className="text-white text-uppercase text-center my-4">
                                            {this.getDateString(this.state.simulatedTime)}
                                        </h1>
                                    </div>
                                    <div
                                        className="col-sm-1 mx-auto p-0 flex justify-content-center align-content-center align-items-center">
                                        <h4 className="text-white text-center">Speed</h4>
                                        <Input
                                            type="text"
                                            name="speed"
                                            value={this.state.speed}
                                            onChange={(e) => this.changeSpeed(`${e.target.value}`)}
                                            placeholder="Enter Speed Value"
                                        />
                                    </div>
                                    <div className="col-md-2 col-sm-1 mx-auto p-0 d-flex justify-content-center">
                                        {this.state.pause ?
                                            (<button className="btn btn-primary mt-4" onClick={this.startSimulation}>
                                                Start Simulation
                                            </button>) :
                                            (<button className="btn btn-danger mt-4" onClick={this.stopSimulation}>
                                                Stop Simulation
                                            </button>)
                                        }
                                    </div>
                                    <div className="col-md-6 col-sm-10 mx-auto p-0 d-flex justify-content-center">
                                        <button className="btn btn-success mt-5 " onClick={this.resetSimulation}>
                                            Reset
                                        </button>
                                    </div>
                                </div>

                            </Col>
                            <Col sm={3}>
                                <div>

                                </div>
                            </Col>
                        </Row>
                    </Container>
                    <div className="">
                        <Console logs={this.state.logs} variant="dark" filter={['log']}/>
                    </div>
                </div>
            </main>
        );
    }
}

export default App;
